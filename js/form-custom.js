$('#quizContain').on('click','.question .valButton', function(){
    let idQuestion = $(this).parents('.question').attr("id-question");
    let valQuiz = parseInt( $(this).parents('span').siblings('.quizValue').val() );
    if($(this).hasClass('moreValue')) valQuiz++;
    if($(this).hasClass('lessValue')) valQuiz--;
    
    if(valQuiz > 0 && valQuiz <= 5)
        $(this).parents('span').siblings('.quizValue').val(valQuiz);
        // $(`.question[id-question="${idQuestion}"] .quizValue`).val(valQuiz);
});
$('.saveQuiz').on("click",function(){
    let JSONQuiz = [];
    let JSONQuestions = [];
    
    $(".question").each(function(index){
        let pregunta = index + 1;
        JSONQuestions.push({
            "preguntas": {
                "pregunta_1": $(this).find("#pregunta"+pregunta+"_1").val(),
                "valor": $(this).find("#valor"+pregunta).val(),
                "correcta_1": $(this).find("#correcta"+pregunta+"_1").val(),
                "incorrecta_1": $(this).find("#incorrecta"+pregunta+"_1").val(),
                "tip": $(this).find("#tip"+pregunta).val(),
                "retroCorrecta": $(this).find("#retroCorrecta"+pregunta).val(),
                "retroIncorrecta": $(this).find("#retroIncorrecta"+pregunta).val(),
                "pregunta_2": $(this).find("#pregunta"+pregunta+"_2").val(),
                "correcta_2": $(this).find("#correcta"+pregunta+"_2").val(),
                "incorrecta_2": $(this).find("#incorrecta"+pregunta+"_2").val(),
                "pregunta_3": $(this).find("#pregunta"+pregunta+"_3").val(),
                "correcta_3": $(this).find("#correcta"+pregunta+"_3").val(),
                "incorrecta_3": $(this).find("#incorrecta"+pregunta+"_3").val()
            }
        });
        JSONQuiz.push({
            "Titulo": $("#titulo").val(),
            "Instrucciones": $("#instrucciones").val(),
            "Preguntas": JSONQuestions
        });
        localStorage.setItem( $("#titulo").val(), JSON.stringify(JSONQuiz) );
    });
    // console.log(JSONQuiz);
    console.log(JSON.parse(localStorage.getItem("Pregunta")));
});
$('.addQuestion').on("click", function(){
    let numQuiz = $(".card.question").length + 1;
    let quiz = `<div class="card card-outline-info question" data-id="${numQuiz}">
                    <div class="card-header">
                        <h4 class="m-b-0">Pregunta ${numQuiz}</h4>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#pregunta${numQuiz}" type="button" role="tab" aria-controls="pregunta${numQuiz}" aria-selected="true">Sobresaliente<br>4</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#opcion${numQuiz}_2" type="button" role="tab" aria-controls="opcion${numQuiz}_2" aria-selected="false">Satisfactorio<br>3</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#opcion${numQuiz}_3" type="button" role="tab" aria-controls="opcion${numQuiz}_3" aria-selected="false">Necesita mejorar<br>2</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#opcion${numQuiz}_4" type="button" role="tab" aria-controls="opcion${numQuiz}_4" aria-selected="false">Insuficiente<br>1</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="pregunta${numQuiz}" role="tabpanel" aria-labelledby="home-tab">
                                <form action="#">
                                    <div class="form-body " id-question="${numQuiz}">
                                        <div class="row p-t-20">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label>Pregunta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe la aquí la pregunta" id="pregunta${numQuiz}_1">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Valor</label>
                                                    <div class="input-group bootstrap-touchspin">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-secondary btn-outline bootstrap-touchspin-down valButton lessValue" type="button">-</button>
                                                        </span>
                                                        <input type="number" value="1" name="tch1" class="form-control quizValue" style="display: block;" min="1" max="5" id="valor${numQuiz}">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-secondary btn-outline bootstrap-touchspin-up valButton moreValue" type="button">+</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe aquí" id="correcta${numQuiz}_1">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_1">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_1_2">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="col-md-4">
                                            <div class="form-check form-switch">
                                                <label class="control-label">Mostrar respuesta</label>
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Tip</label>
                                                    <input type="text" class="form-control" placeholder="Escribe aquí" id="tip${numQuiz}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Retro respuesta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe aquí" id="retroPositiva${numQuiz}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Retro respuesta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe aquí" id="retroNegativa${numQuiz}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="opcion${numQuiz}_2" role="tabpanel" aria-labelledby="profile-tab">
                                <form action="#">
                                    <div class="form-body try">
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Pregunta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe la aquí la pregunta" id="pregunta${numQuiz}_2">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe aquí" id="correcta${numQuiz}_2">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_2">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_2_2">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="opcion${numQuiz}_3" role="tabpanel" aria-labelledby="contact-tab">
                                <form action="#">
                                    <div class="form-body try">
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Pregunta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe la aquí la pregunta"id="pregunta${numQuiz}_3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe aquí" id="correcta${numQuiz}_3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_3_2">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="opcion${numQuiz}_4" role="tabpanel" aria-labelledby="contact-tab">
                                <form action="#">
                                    <div class="form-body try">
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Pregunta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe la aquí la pregunta"id="pregunta${numQuiz}_4">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control" placeholder="Escribe aquí" id="correcta${numQuiz}_4">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_4">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group has-danger">
                                                    <label class="control-label">Respuesta</label>
                                                    <input type="text" class="form-control form-control-danger" placeholder="Escribe aquí" id="incorrecta${numQuiz}_4_2">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br>`;
    $('#quizContain').append(quiz);
});